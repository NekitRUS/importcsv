<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once 'DataBase.php';
        DataBase::getInstance()->UploadUsers(__DIR__ . "\contactList.csv");
        $userList = DataBase::getInstance()->GetUsers();
        echo "<center><H1>Список зарегистрированных пользователей:</H1>";
        echo "<table border = black>";
        echo "<tr><th>ФИО</th><th>E-mail</th><th>Дата регистрации</th><th>Хеш пароля</th></tr>";
        while($row = mysqli_fetch_array($userList)){
            echo "<tr>";
            echo "<td>" . $row['FIO'] . "</td>";
            echo "<td><a href=mailto:" . $row['email'] . ">Написать письмо</a></td>";
            echo "<td>" . $row['registrationDate'] . "</td>";
            echo "<td>" . $row['password'] . "</td>";
            echo "</tr>";
        }
        echo "</table>"
        ?>
    </body>
</html>
