<?php
class DataBase extends mysqli {
    
    private static $instance = null;

    private $user = "phpuser";
    private $pass = "phpuserpw";
    private $dbName = "users";
    private $dbHost = "localhost";
    
    private function __construct() {
        parent::__construct($this->dbHost, $this->user, $this->pass, $this->dbName);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        parent::set_charset('utf-8');
    }
    
    public static function getInstance() {
       if (self::$instance === null) {
         self::$instance = new static();
       }
       return self::$instance;
     }
     
    private function __clone() {
    }

    private function __wakeup() {
    }
     
    public function UploadUsers($pathToCSV){
        if ((!file_exists($pathToCSV)) || (substr($pathToCSV, -4)!== '.csv')){
            exit("Неверный путь к файлу!");
        }
        
        $file = fopen($pathToCSV, 'r');
        if ($file == FALSE){
            exit("Невозможно открыть файл!");
        }
        
        while (($userInfo = fgetcsv($file, 0, ";")) !== FALSE){
            $this->QueryToAdd($userInfo);
        }
        fclose($file);
    }
     
    private function GetRandomString(){
        $characters = '!@#$%^&*()_+№;:?-=0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < rand(6, 12); $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
     
    private function Encrypt($str){
        $salt = 'myProtecion';
        return md5(strrev(md5($salt . $str)));
    }
    
    private function FormatDateForSQL($date){
        $dateParts = date_parse(date_format($date, "d-m-Y"));
        return $dateParts["year"]*10000 + $dateParts["month"]*100 + $dateParts["day"];
    }
    
    private function QueryToAdd($data){
        $name = $this->real_escape_string($data[0]);
        $email = $this->real_escape_string($data[1]);
        $date = date_create($data[2]);
        $password = $this->Encrypt($this->GetRandomString());
        if (mysqli_num_rows($this->query("SELECT * FROM userlist WHERE email='" . $email . "'")) > 0){ return; }
        $this->query("INSERT INTO userlist (FIO, email, registrationDate, password) VALUES ('" . $name . "', '" . $email . "', '" . $this->FormatDateForSQL($date) . "', '" . $password . "')");
    }
    
    public function GetUsers(){
        return $this->query("SELECT * FROM userlist");
    }
     
}
